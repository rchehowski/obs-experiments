#include <stdio.h>
#include <stdlib.h>
#include <string.h>

constexpr char ArrayVarName[] = "payload_bytes";
constexpr uint64_t BytesInLine = 20;
constexpr const char* OutFileName = "payload.js";

int main(int argc, const char * argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "Run this program passing payload as CLI parameter\n");
		fflush(stderr);
		exit(1);
	}
	
	const char* const inFileName = argv[1];
	
	FILE* const inFile = fopen(inFileName, "rb");
	if (inFile == nullptr)
	{
		fprintf(stderr, "Can not open input file: \"%s\"\n", inFileName);
		fflush(stderr);
		exit(1);
	}
	
	fseek(inFile, 0, SEEK_END);
	const uint64_t fileSize = ftell(inFile);
	fseek(inFile, 0, SEEK_SET);
	
	printf("Input file is \"%s\", %llu bytes\n", inFileName, fileSize);
	
	FILE* outFile = fopen(OutFileName, "w");
	if (outFile == nullptr)
	{
		fprintf(stderr, "Can not open output file: \"%s\"\n", OutFileName);
		fflush(stderr);
		exit(1);
	}
	
	printf("Writing content into \"%s\"... ", OutFileName);
	
	const char* simpleFileName = nullptr;
	{
		constexpr char pathSeparators[] = { '/', '\\' };
		constexpr int numPathSeparators = sizeof(pathSeparators) / sizeof(*pathSeparators);
		
		for (int i = 0; (i < numPathSeparators) && (simpleFileName == nullptr); ++i)
		{
			simpleFileName = strrchr(inFileName, pathSeparators[i]);
		}
		
		if (simpleFileName != nullptr)
			simpleFileName++;
	}
	
	// variable comment
	fprintf(outFile, "/** %s (%llu bytes) */\n", (simpleFileName ? simpleFileName : inFileName), fileSize);
	
	fprintf(outFile, "let %s = [\n", ArrayVarName);
	
	for (uint64_t index = 0; index < fileSize; ++index)
	{
		if ((index != 0) && (index % BytesInLine == 0))
		{
			fprintf(outFile, "\n");
		}
		
		unsigned char byte;
		fread(&byte, sizeof(byte), 1, inFile);
		
		const bool bLastOne = (index == (fileSize - 1));
		fprintf(outFile, "0x%02x%s", (int)byte, bLastOne ? "" : ", ");
	}
	
	fprintf(outFile, "\n];\n");
	
	printf("Done!\n");
	
	fclose(inFile);
	fclose(outFile);
}
